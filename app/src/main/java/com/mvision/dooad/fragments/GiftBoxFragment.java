package com.mvision.dooad.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mvision.dooad.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GiftBoxFragment extends Fragment {


    public GiftBoxFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gift_box, container, false);
    }


}
