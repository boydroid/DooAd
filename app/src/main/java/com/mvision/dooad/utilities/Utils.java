package com.mvision.dooad.utilities;

import android.app.Activity;
import android.app.Fragment;

import com.mvision.dooad.R;

/**
 * Created by boy on 18/9/2558.
 */
public class Utils {
    public static void startFragment(Activity activity, Fragment fragment, boolean addBackStack) {
        if (addBackStack) {
            activity.getFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
        } else {
            activity.getFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();
        }
    }
}
