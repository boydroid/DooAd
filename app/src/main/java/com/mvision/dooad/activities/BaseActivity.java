package com.mvision.dooad.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.mvision.dooad.R;

/**
 * Created by boy on 18/9/2558.
 */
public abstract class BaseActivity extends Activity implements View.OnClickListener{

    protected abstract int setContentView();
    public abstract void setNavigationBar(TextView left, TextView title, TextView right);
    public TextView textNavLeft;
    public TextView textNavTitle;
    public TextView textNavRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setContentView());

        setActionBar();
    }

    private void setActionBar() {
        getActionBar().setCustomView(R.layout.layout_navagation_bar);
        getActionBar().setDisplayHomeAsUpEnabled(false);
        getActionBar().setDisplayShowCustomEnabled(true);
        //btnHome set icon and back
        getActionBar().setHomeButtonEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(false);

        textNavLeft = (TextView) getActionBar().getCustomView().findViewById(R.id.textNavLeft);
        textNavTitle = (TextView) getActionBar().getCustomView().findViewById(R.id.textNavTitle);
        textNavRight = (TextView) getActionBar().getCustomView().findViewById(R.id.textNavRight);

        textNavLeft.setOnClickListener(this);
        textNavRight.setOnClickListener(this);

        setNavigationBar(textNavLeft, textNavTitle, textNavRight);
    }
}
